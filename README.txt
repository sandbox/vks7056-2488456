Device detect block module provide a tab to configure block visiability at Device
(tablet) device as well as give the facility to showblock in different region at
Device device. It keeps the configuration in its created table in database.

Dependencies
------------

Device detect block requires browscap module to work.


Requirements
------------

Device detect block requires that your server be able to make a http request
to retrieve client user agent detail.

Installation
------------

Device detect can be installed via the standard Drupal installation process.
http://drupal.org/node/895232

